-- public.meeting_rooms definition

-- Drop table

-- DROP TABLE public.meeting_rooms;

CREATE TABLE public.meeting_rooms (
	id serial NOT NULL,
	"name" varchar(255) NOT NULL,
	capacity int4 NOT NULL,
	description text NULL,
	ubication varchar(255) NULL,
	CONSTRAINT meeting_rooms_pkey PRIMARY KEY (id)
);


-- public.resources definition

-- Drop table

-- DROP TABLE public.resources;

CREATE TABLE public.resources (
	id serial NOT NULL,
	"name" varchar(255) NOT NULL,
	description text NULL,
	CONSTRAINT resources_pkey PRIMARY KEY (id)
);


-- public.teams definition

-- Drop table

-- DROP TABLE public.teams;

CREATE TABLE public.teams (
	id serial NOT NULL,
	"name" varchar(255) NOT NULL,
	description text NULL,
	CONSTRAINT teams_pkey PRIMARY KEY (id)
);


-- public.reservations definition

-- Drop table

-- DROP TABLE public.reservations;

CREATE TABLE public.reservations (
	id serial NOT NULL,
	meeting_room_id int4 NULL,
	user_identifier varchar(255) NULL,
	team_id int4 NULL,
	start_time timestamp NOT NULL,
	end_time timestamp NOT NULL,
	title varchar(255) NOT NULL,
	description text NULL,
	purpose varchar(255) NULL,
	attendees_number int4 NULL,
	cancelled bool NULL DEFAULT false,
	CONSTRAINT reservations_check CHECK (((user_identifier IS NOT NULL) OR (team_id IS NOT NULL))),
	CONSTRAINT reservations_meeting_room_id_start_time_end_time_key UNIQUE (meeting_room_id, start_time, end_time),
	CONSTRAINT reservations_pkey PRIMARY KEY (id),
	CONSTRAINT reservations_meeting_room_id_fkey FOREIGN KEY (meeting_room_id) REFERENCES meeting_rooms(id) ON DELETE CASCADE,
	CONSTRAINT reservations_team_id_fkey FOREIGN KEY (team_id) REFERENCES teams(id)
);


-- public.resource_usage definition

-- Drop table

-- DROP TABLE public.resource_usage;

CREATE TABLE public.resource_usage (
	id serial NOT NULL,
	reservation_id int4 NULL,
	resource_id int4 NULL,
	quantity_used int4 NOT NULL,
	CONSTRAINT resource_usage_pkey PRIMARY KEY (id),
	CONSTRAINT resource_usage_reservation_id_fkey FOREIGN KEY (reservation_id) REFERENCES reservations(id) ON DELETE CASCADE,
	CONSTRAINT resource_usage_resource_id_fkey FOREIGN KEY (resource_id) REFERENCES resources(id) ON DELETE CASCADE
);


-- public.resources_room definition

-- Drop table

-- DROP TABLE public.resources_room;

CREATE TABLE public.resources_room (
	id serial NOT NULL,
	meeting_room_id int4 NULL,
	resource_id int4 NULL,
	quantity int4 NOT NULL,
	CONSTRAINT resources_room_meeting_room_id_resource_id_key UNIQUE (meeting_room_id, resource_id),
	CONSTRAINT resources_room_pkey PRIMARY KEY (id),
	CONSTRAINT resources_room_meeting_room_id_fkey FOREIGN KEY (meeting_room_id) REFERENCES meeting_rooms(id) ON DELETE CASCADE,
	CONSTRAINT resources_room_resource_id_fkey FOREIGN KEY (resource_id) REFERENCES resources(id) ON DELETE CASCADE
);


-- public.room_images definition

-- Drop table

-- DROP TABLE public.room_images;

CREATE TABLE public.room_images (
	id serial NOT NULL,
	meeting_room_id int4 NULL,
	image bytea NOT NULL,
	description text NULL,
	CONSTRAINT room_images_pkey PRIMARY KEY (id),
	CONSTRAINT room_images_meeting_room_id_fkey FOREIGN KEY (meeting_room_id) REFERENCES meeting_rooms(id) ON DELETE CASCADE
);

-- DROP TABLE IF EXISTS users CASCADE;
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    cusca_id VARCHAR(9) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    is_active BOOLEAN NOT NULL DEFAULT TRUE
);

-- DROP TABLE IF EXISTS roles CASCADE;
CREATE TABLE roles (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) UNIQUE NOT NULL,
    description VARCHAR(255)
);

-- DROP TABLE IF EXISTS permissions CASCADE;
CREATE TABLE permissions (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) UNIQUE NOT NULL,
    description VARCHAR(255)
);

-- DROP TABLE IF EXISTS users_roles CASCADE;
CREATE TABLE users_roles (
    user_id INTEGER NOT NULL REFERENCES users(id),
    role_id INTEGER NOT NULL REFERENCES roles(id),
    PRIMARY KEY (user_id, role_id)
);

-- DROP TABLE IF EXISTS roles_permissions CASCADE;
CREATE TABLE roles_permissions (
    role_id INTEGER NOT NULL REFERENCES roles(id),
    permission_id INTEGER NOT NULL REFERENCES permissions(id),
    PRIMARY KEY (role_id, permission_id)
);


INSERT INTO public.meeting_rooms ("name", capacity, description, ubication) VALUES
('Sala Ilopango', 25, 'Sala de reuniones Ilopango', 'Edificio Piramide, Nivel 1'),
('Sala Coatepeque', 15, 'Sala de reuniones Coatepeque', 'Edificio Piramide, Nivel 1'),
('Sala Moncagua', 15, 'Sala de reuniones', 'Edificio Piramide, Nivel 2');

INSERT INTO public.resources ("name", description) VALUES
('TV LG Smart', 'TV LG Smart 69 pulgadas'),
('Sillas', 'Sillas semi ejecutivas'),
('Proyector', 'Proyector con HDMI, VGA y WiFi');

INSERT INTO public.teams ("name", description) VALUES
('Evolution Team', 'Equipo de Apificacion');

INSERT INTO public.resources_room (meeting_room_id, resource_id, quantity) VALUES
(1, 1, 1),
(1, 2, 25),
(2, 2, 25),
(2, 3, 1),
(3, 2, 25),
(3, 3, 1);

INSERT INTO public.reservations (meeting_room_id,user_identifier,team_id,start_time,end_time,title,description,purpose,attendees_number,cancelled) VALUES
	 (1,NULL,1,'2023-03-26 00:34:31.911','2023-03-26 00:34:31.911','hackaton','prueba','Reunión de prueba',NULL,false),
	 (2,NULL,1,'2023-03-26 00:48:27.592','2023-03-26 00:48:27.592','hackaton','prueba','Reunión de prueba',NULL,false),
	 (3,NULL,1,'2023-03-26 01:00:10.72','2023-03-26 01:09:36.671','hackaton','prueba','Reunión de prueba',NULL,false),
	 (1,NULL,1,'2023-03-26 01:07:10.72','2023-03-26 01:18:21.825','hackaton','prueba','Reunión de prueba',NULL,false),
	 (2,NULL,1,'2023-03-26 01:51:31.911','2023-03-26 02:51:31.911','hackaton','prueba','Reunión de prueba',NULL,false),
	 (3,NULL,1,'2023-03-26 00:49:10.72','2023-03-26 01:49:10.72','hackaton','prueba','Reunión de prueba',NULL,true);

