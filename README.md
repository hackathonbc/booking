# Overview

_Nombre del proyecto:_ Booking.

_Requisitos:_

- [Docker](https://www.docker.com/get-started/). 
- Docker Compose
- Terminal de comandos.
- Navegador web.

##### ***Pasos para ejecutar el proyecto:***

* Descargar o clonar este repositorio en su computador.
* Abrir una terminal de comandos.
* En la terminal de comando ubicarse en el directorio ***RUTA_DESCARGA_DEL_PROYECTO/booking***.
* Ejecutar en la terminal de comandos la siguiente instruccion: ***docker-compose up -d***.
* Abrir el navegador y digitar la URL: ***http://localhost:8080/***.

##### ***Equipo de desarrollo:***

* Mario Gerardo Martinez.
* Christian Vladimir Avalos.
* José David Artiga.
